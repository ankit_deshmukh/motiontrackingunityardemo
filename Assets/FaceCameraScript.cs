﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceCameraScript : MonoBehaviour
{
   public GameObject go;

    private Vector3 camPos, goPos;

    // Start is called before the first frame update
    void Start()
    { 

    }

    // Update is called once per frame
    void Update()
    {
        camPos = Camera.main.transform.position;
        Debug.Log("Camera Position = " + camPos);

        goPos = go.transform.position;
        Debug.Log("Game object position = " + goPos);

        go.transform.LookAt(Camera.main.transform);

        goPos.z = camPos.z + 3;

        go.transform.position = goPos;
        Debug.Log("Updated position of Game Object = " + go.transform.position);
    }
}
