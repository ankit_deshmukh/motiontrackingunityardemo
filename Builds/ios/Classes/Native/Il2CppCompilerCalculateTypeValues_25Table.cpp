﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityChan.SpringBone[]
struct SpringBoneU5BU5D_t9AF3CD074735AB2EC488E7D9B595D1ACBAABBC0E;
// UnityChan.SpringCollider[]
struct SpringColliderU5BU5D_tD22CDC9AE9650D865E759598C7272E4719A13F2B;
// UnityChan.SpringManager
struct SpringManager_t6A624FA45C3EA6326C3B0A4B34EF92F7C41D15DE;
// UnityEngine.AnimationCurve
struct AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C;
// UnityEngine.Animator
struct Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;




#ifndef U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#define U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef FACECAMERASCRIPT_T0AB5EB4E42EB5C0A6C833626C3A892612E1A94BD_H
#define FACECAMERASCRIPT_T0AB5EB4E42EB5C0A6C833626C3A892612E1A94BD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FaceCameraScript
struct  FaceCameraScript_t0AB5EB4E42EB5C0A6C833626C3A892612E1A94BD  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject FaceCameraScript::go
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___go_4;
	// UnityEngine.Vector3 FaceCameraScript::camPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___camPos_5;
	// UnityEngine.Vector3 FaceCameraScript::goPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___goPos_6;

public:
	inline static int32_t get_offset_of_go_4() { return static_cast<int32_t>(offsetof(FaceCameraScript_t0AB5EB4E42EB5C0A6C833626C3A892612E1A94BD, ___go_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_go_4() const { return ___go_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_go_4() { return &___go_4; }
	inline void set_go_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___go_4 = value;
		Il2CppCodeGenWriteBarrier((&___go_4), value);
	}

	inline static int32_t get_offset_of_camPos_5() { return static_cast<int32_t>(offsetof(FaceCameraScript_t0AB5EB4E42EB5C0A6C833626C3A892612E1A94BD, ___camPos_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_camPos_5() const { return ___camPos_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_camPos_5() { return &___camPos_5; }
	inline void set_camPos_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___camPos_5 = value;
	}

	inline static int32_t get_offset_of_goPos_6() { return static_cast<int32_t>(offsetof(FaceCameraScript_t0AB5EB4E42EB5C0A6C833626C3A892612E1A94BD, ___goPos_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_goPos_6() const { return ___goPos_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_goPos_6() { return &___goPos_6; }
	inline void set_goPos_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___goPos_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACECAMERASCRIPT_T0AB5EB4E42EB5C0A6C833626C3A892612E1A94BD_H
#ifndef SAMPLEANIMATION_TD33B7626F8F2B0967D4D33E5C94D015DA285C1AC_H
#define SAMPLEANIMATION_TD33B7626F8F2B0967D4D33E5C94D015DA285C1AC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SampleAnimation
struct  SampleAnimation_tD33B7626F8F2B0967D4D33E5C94D015DA285C1AC  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Animator SampleAnimation::animator
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___animator_4;

public:
	inline static int32_t get_offset_of_animator_4() { return static_cast<int32_t>(offsetof(SampleAnimation_tD33B7626F8F2B0967D4D33E5C94D015DA285C1AC, ___animator_4)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_animator_4() const { return ___animator_4; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_animator_4() { return &___animator_4; }
	inline void set_animator_4(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___animator_4 = value;
		Il2CppCodeGenWriteBarrier((&___animator_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLEANIMATION_TD33B7626F8F2B0967D4D33E5C94D015DA285C1AC_H
#ifndef SPRINGBONE_T622CCCF9297DA4FE229847C281D2A4B3737F7543_H
#define SPRINGBONE_T622CCCF9297DA4FE229847C281D2A4B3737F7543_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityChan.SpringBone
struct  SpringBone_t622CCCF9297DA4FE229847C281D2A4B3737F7543  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Transform UnityChan.SpringBone::child
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___child_4;
	// UnityEngine.Vector3 UnityChan.SpringBone::boneAxis
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___boneAxis_5;
	// System.Single UnityChan.SpringBone::radius
	float ___radius_6;
	// System.Boolean UnityChan.SpringBone::isUseEachBoneForceSettings
	bool ___isUseEachBoneForceSettings_7;
	// System.Single UnityChan.SpringBone::stiffnessForce
	float ___stiffnessForce_8;
	// System.Single UnityChan.SpringBone::dragForce
	float ___dragForce_9;
	// UnityEngine.Vector3 UnityChan.SpringBone::springForce
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___springForce_10;
	// UnityChan.SpringCollider[] UnityChan.SpringBone::colliders
	SpringColliderU5BU5D_tD22CDC9AE9650D865E759598C7272E4719A13F2B* ___colliders_11;
	// System.Boolean UnityChan.SpringBone::debug
	bool ___debug_12;
	// System.Single UnityChan.SpringBone::threshold
	float ___threshold_13;
	// System.Single UnityChan.SpringBone::springLength
	float ___springLength_14;
	// UnityEngine.Quaternion UnityChan.SpringBone::localRotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___localRotation_15;
	// UnityEngine.Transform UnityChan.SpringBone::trs
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___trs_16;
	// UnityEngine.Vector3 UnityChan.SpringBone::currTipPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___currTipPos_17;
	// UnityEngine.Vector3 UnityChan.SpringBone::prevTipPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___prevTipPos_18;
	// UnityEngine.Transform UnityChan.SpringBone::org
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___org_19;
	// UnityChan.SpringManager UnityChan.SpringBone::managerRef
	SpringManager_t6A624FA45C3EA6326C3B0A4B34EF92F7C41D15DE * ___managerRef_20;

public:
	inline static int32_t get_offset_of_child_4() { return static_cast<int32_t>(offsetof(SpringBone_t622CCCF9297DA4FE229847C281D2A4B3737F7543, ___child_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_child_4() const { return ___child_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_child_4() { return &___child_4; }
	inline void set_child_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___child_4 = value;
		Il2CppCodeGenWriteBarrier((&___child_4), value);
	}

	inline static int32_t get_offset_of_boneAxis_5() { return static_cast<int32_t>(offsetof(SpringBone_t622CCCF9297DA4FE229847C281D2A4B3737F7543, ___boneAxis_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_boneAxis_5() const { return ___boneAxis_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_boneAxis_5() { return &___boneAxis_5; }
	inline void set_boneAxis_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___boneAxis_5 = value;
	}

	inline static int32_t get_offset_of_radius_6() { return static_cast<int32_t>(offsetof(SpringBone_t622CCCF9297DA4FE229847C281D2A4B3737F7543, ___radius_6)); }
	inline float get_radius_6() const { return ___radius_6; }
	inline float* get_address_of_radius_6() { return &___radius_6; }
	inline void set_radius_6(float value)
	{
		___radius_6 = value;
	}

	inline static int32_t get_offset_of_isUseEachBoneForceSettings_7() { return static_cast<int32_t>(offsetof(SpringBone_t622CCCF9297DA4FE229847C281D2A4B3737F7543, ___isUseEachBoneForceSettings_7)); }
	inline bool get_isUseEachBoneForceSettings_7() const { return ___isUseEachBoneForceSettings_7; }
	inline bool* get_address_of_isUseEachBoneForceSettings_7() { return &___isUseEachBoneForceSettings_7; }
	inline void set_isUseEachBoneForceSettings_7(bool value)
	{
		___isUseEachBoneForceSettings_7 = value;
	}

	inline static int32_t get_offset_of_stiffnessForce_8() { return static_cast<int32_t>(offsetof(SpringBone_t622CCCF9297DA4FE229847C281D2A4B3737F7543, ___stiffnessForce_8)); }
	inline float get_stiffnessForce_8() const { return ___stiffnessForce_8; }
	inline float* get_address_of_stiffnessForce_8() { return &___stiffnessForce_8; }
	inline void set_stiffnessForce_8(float value)
	{
		___stiffnessForce_8 = value;
	}

	inline static int32_t get_offset_of_dragForce_9() { return static_cast<int32_t>(offsetof(SpringBone_t622CCCF9297DA4FE229847C281D2A4B3737F7543, ___dragForce_9)); }
	inline float get_dragForce_9() const { return ___dragForce_9; }
	inline float* get_address_of_dragForce_9() { return &___dragForce_9; }
	inline void set_dragForce_9(float value)
	{
		___dragForce_9 = value;
	}

	inline static int32_t get_offset_of_springForce_10() { return static_cast<int32_t>(offsetof(SpringBone_t622CCCF9297DA4FE229847C281D2A4B3737F7543, ___springForce_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_springForce_10() const { return ___springForce_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_springForce_10() { return &___springForce_10; }
	inline void set_springForce_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___springForce_10 = value;
	}

	inline static int32_t get_offset_of_colliders_11() { return static_cast<int32_t>(offsetof(SpringBone_t622CCCF9297DA4FE229847C281D2A4B3737F7543, ___colliders_11)); }
	inline SpringColliderU5BU5D_tD22CDC9AE9650D865E759598C7272E4719A13F2B* get_colliders_11() const { return ___colliders_11; }
	inline SpringColliderU5BU5D_tD22CDC9AE9650D865E759598C7272E4719A13F2B** get_address_of_colliders_11() { return &___colliders_11; }
	inline void set_colliders_11(SpringColliderU5BU5D_tD22CDC9AE9650D865E759598C7272E4719A13F2B* value)
	{
		___colliders_11 = value;
		Il2CppCodeGenWriteBarrier((&___colliders_11), value);
	}

	inline static int32_t get_offset_of_debug_12() { return static_cast<int32_t>(offsetof(SpringBone_t622CCCF9297DA4FE229847C281D2A4B3737F7543, ___debug_12)); }
	inline bool get_debug_12() const { return ___debug_12; }
	inline bool* get_address_of_debug_12() { return &___debug_12; }
	inline void set_debug_12(bool value)
	{
		___debug_12 = value;
	}

	inline static int32_t get_offset_of_threshold_13() { return static_cast<int32_t>(offsetof(SpringBone_t622CCCF9297DA4FE229847C281D2A4B3737F7543, ___threshold_13)); }
	inline float get_threshold_13() const { return ___threshold_13; }
	inline float* get_address_of_threshold_13() { return &___threshold_13; }
	inline void set_threshold_13(float value)
	{
		___threshold_13 = value;
	}

	inline static int32_t get_offset_of_springLength_14() { return static_cast<int32_t>(offsetof(SpringBone_t622CCCF9297DA4FE229847C281D2A4B3737F7543, ___springLength_14)); }
	inline float get_springLength_14() const { return ___springLength_14; }
	inline float* get_address_of_springLength_14() { return &___springLength_14; }
	inline void set_springLength_14(float value)
	{
		___springLength_14 = value;
	}

	inline static int32_t get_offset_of_localRotation_15() { return static_cast<int32_t>(offsetof(SpringBone_t622CCCF9297DA4FE229847C281D2A4B3737F7543, ___localRotation_15)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_localRotation_15() const { return ___localRotation_15; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_localRotation_15() { return &___localRotation_15; }
	inline void set_localRotation_15(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___localRotation_15 = value;
	}

	inline static int32_t get_offset_of_trs_16() { return static_cast<int32_t>(offsetof(SpringBone_t622CCCF9297DA4FE229847C281D2A4B3737F7543, ___trs_16)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_trs_16() const { return ___trs_16; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_trs_16() { return &___trs_16; }
	inline void set_trs_16(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___trs_16 = value;
		Il2CppCodeGenWriteBarrier((&___trs_16), value);
	}

	inline static int32_t get_offset_of_currTipPos_17() { return static_cast<int32_t>(offsetof(SpringBone_t622CCCF9297DA4FE229847C281D2A4B3737F7543, ___currTipPos_17)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_currTipPos_17() const { return ___currTipPos_17; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_currTipPos_17() { return &___currTipPos_17; }
	inline void set_currTipPos_17(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___currTipPos_17 = value;
	}

	inline static int32_t get_offset_of_prevTipPos_18() { return static_cast<int32_t>(offsetof(SpringBone_t622CCCF9297DA4FE229847C281D2A4B3737F7543, ___prevTipPos_18)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_prevTipPos_18() const { return ___prevTipPos_18; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_prevTipPos_18() { return &___prevTipPos_18; }
	inline void set_prevTipPos_18(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___prevTipPos_18 = value;
	}

	inline static int32_t get_offset_of_org_19() { return static_cast<int32_t>(offsetof(SpringBone_t622CCCF9297DA4FE229847C281D2A4B3737F7543, ___org_19)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_org_19() const { return ___org_19; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_org_19() { return &___org_19; }
	inline void set_org_19(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___org_19 = value;
		Il2CppCodeGenWriteBarrier((&___org_19), value);
	}

	inline static int32_t get_offset_of_managerRef_20() { return static_cast<int32_t>(offsetof(SpringBone_t622CCCF9297DA4FE229847C281D2A4B3737F7543, ___managerRef_20)); }
	inline SpringManager_t6A624FA45C3EA6326C3B0A4B34EF92F7C41D15DE * get_managerRef_20() const { return ___managerRef_20; }
	inline SpringManager_t6A624FA45C3EA6326C3B0A4B34EF92F7C41D15DE ** get_address_of_managerRef_20() { return &___managerRef_20; }
	inline void set_managerRef_20(SpringManager_t6A624FA45C3EA6326C3B0A4B34EF92F7C41D15DE * value)
	{
		___managerRef_20 = value;
		Il2CppCodeGenWriteBarrier((&___managerRef_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRINGBONE_T622CCCF9297DA4FE229847C281D2A4B3737F7543_H
#ifndef SPRINGCOLLIDER_TDE193DFAA6219FEF2706546DF578770FB3744C2E_H
#define SPRINGCOLLIDER_TDE193DFAA6219FEF2706546DF578770FB3744C2E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityChan.SpringCollider
struct  SpringCollider_tDE193DFAA6219FEF2706546DF578770FB3744C2E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single UnityChan.SpringCollider::radius
	float ___radius_4;

public:
	inline static int32_t get_offset_of_radius_4() { return static_cast<int32_t>(offsetof(SpringCollider_tDE193DFAA6219FEF2706546DF578770FB3744C2E, ___radius_4)); }
	inline float get_radius_4() const { return ___radius_4; }
	inline float* get_address_of_radius_4() { return &___radius_4; }
	inline void set_radius_4(float value)
	{
		___radius_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRINGCOLLIDER_TDE193DFAA6219FEF2706546DF578770FB3744C2E_H
#ifndef SPRINGMANAGER_T6A624FA45C3EA6326C3B0A4B34EF92F7C41D15DE_H
#define SPRINGMANAGER_T6A624FA45C3EA6326C3B0A4B34EF92F7C41D15DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityChan.SpringManager
struct  SpringManager_t6A624FA45C3EA6326C3B0A4B34EF92F7C41D15DE  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single UnityChan.SpringManager::dynamicRatio
	float ___dynamicRatio_4;
	// System.Single UnityChan.SpringManager::stiffnessForce
	float ___stiffnessForce_5;
	// UnityEngine.AnimationCurve UnityChan.SpringManager::stiffnessCurve
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___stiffnessCurve_6;
	// System.Single UnityChan.SpringManager::dragForce
	float ___dragForce_7;
	// UnityEngine.AnimationCurve UnityChan.SpringManager::dragCurve
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___dragCurve_8;
	// UnityChan.SpringBone[] UnityChan.SpringManager::springBones
	SpringBoneU5BU5D_t9AF3CD074735AB2EC488E7D9B595D1ACBAABBC0E* ___springBones_9;

public:
	inline static int32_t get_offset_of_dynamicRatio_4() { return static_cast<int32_t>(offsetof(SpringManager_t6A624FA45C3EA6326C3B0A4B34EF92F7C41D15DE, ___dynamicRatio_4)); }
	inline float get_dynamicRatio_4() const { return ___dynamicRatio_4; }
	inline float* get_address_of_dynamicRatio_4() { return &___dynamicRatio_4; }
	inline void set_dynamicRatio_4(float value)
	{
		___dynamicRatio_4 = value;
	}

	inline static int32_t get_offset_of_stiffnessForce_5() { return static_cast<int32_t>(offsetof(SpringManager_t6A624FA45C3EA6326C3B0A4B34EF92F7C41D15DE, ___stiffnessForce_5)); }
	inline float get_stiffnessForce_5() const { return ___stiffnessForce_5; }
	inline float* get_address_of_stiffnessForce_5() { return &___stiffnessForce_5; }
	inline void set_stiffnessForce_5(float value)
	{
		___stiffnessForce_5 = value;
	}

	inline static int32_t get_offset_of_stiffnessCurve_6() { return static_cast<int32_t>(offsetof(SpringManager_t6A624FA45C3EA6326C3B0A4B34EF92F7C41D15DE, ___stiffnessCurve_6)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_stiffnessCurve_6() const { return ___stiffnessCurve_6; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_stiffnessCurve_6() { return &___stiffnessCurve_6; }
	inline void set_stiffnessCurve_6(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___stiffnessCurve_6 = value;
		Il2CppCodeGenWriteBarrier((&___stiffnessCurve_6), value);
	}

	inline static int32_t get_offset_of_dragForce_7() { return static_cast<int32_t>(offsetof(SpringManager_t6A624FA45C3EA6326C3B0A4B34EF92F7C41D15DE, ___dragForce_7)); }
	inline float get_dragForce_7() const { return ___dragForce_7; }
	inline float* get_address_of_dragForce_7() { return &___dragForce_7; }
	inline void set_dragForce_7(float value)
	{
		___dragForce_7 = value;
	}

	inline static int32_t get_offset_of_dragCurve_8() { return static_cast<int32_t>(offsetof(SpringManager_t6A624FA45C3EA6326C3B0A4B34EF92F7C41D15DE, ___dragCurve_8)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_dragCurve_8() const { return ___dragCurve_8; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_dragCurve_8() { return &___dragCurve_8; }
	inline void set_dragCurve_8(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___dragCurve_8 = value;
		Il2CppCodeGenWriteBarrier((&___dragCurve_8), value);
	}

	inline static int32_t get_offset_of_springBones_9() { return static_cast<int32_t>(offsetof(SpringManager_t6A624FA45C3EA6326C3B0A4B34EF92F7C41D15DE, ___springBones_9)); }
	inline SpringBoneU5BU5D_t9AF3CD074735AB2EC488E7D9B595D1ACBAABBC0E* get_springBones_9() const { return ___springBones_9; }
	inline SpringBoneU5BU5D_t9AF3CD074735AB2EC488E7D9B595D1ACBAABBC0E** get_address_of_springBones_9() { return &___springBones_9; }
	inline void set_springBones_9(SpringBoneU5BU5D_t9AF3CD074735AB2EC488E7D9B595D1ACBAABBC0E* value)
	{
		___springBones_9 = value;
		Il2CppCodeGenWriteBarrier((&___springBones_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRINGMANAGER_T6A624FA45C3EA6326C3B0A4B34EF92F7C41D15DE_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2500 = { sizeof (U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2501 = { sizeof (FaceCameraScript_t0AB5EB4E42EB5C0A6C833626C3A892612E1A94BD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2501[3] = 
{
	FaceCameraScript_t0AB5EB4E42EB5C0A6C833626C3A892612E1A94BD::get_offset_of_go_4(),
	FaceCameraScript_t0AB5EB4E42EB5C0A6C833626C3A892612E1A94BD::get_offset_of_camPos_5(),
	FaceCameraScript_t0AB5EB4E42EB5C0A6C833626C3A892612E1A94BD::get_offset_of_goPos_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2502 = { sizeof (SampleAnimation_tD33B7626F8F2B0967D4D33E5C94D015DA285C1AC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2502[7] = 
{
	SampleAnimation_tD33B7626F8F2B0967D4D33E5C94D015DA285C1AC::get_offset_of_animator_4(),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2503 = { sizeof (SpringBone_t622CCCF9297DA4FE229847C281D2A4B3737F7543), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2503[17] = 
{
	SpringBone_t622CCCF9297DA4FE229847C281D2A4B3737F7543::get_offset_of_child_4(),
	SpringBone_t622CCCF9297DA4FE229847C281D2A4B3737F7543::get_offset_of_boneAxis_5(),
	SpringBone_t622CCCF9297DA4FE229847C281D2A4B3737F7543::get_offset_of_radius_6(),
	SpringBone_t622CCCF9297DA4FE229847C281D2A4B3737F7543::get_offset_of_isUseEachBoneForceSettings_7(),
	SpringBone_t622CCCF9297DA4FE229847C281D2A4B3737F7543::get_offset_of_stiffnessForce_8(),
	SpringBone_t622CCCF9297DA4FE229847C281D2A4B3737F7543::get_offset_of_dragForce_9(),
	SpringBone_t622CCCF9297DA4FE229847C281D2A4B3737F7543::get_offset_of_springForce_10(),
	SpringBone_t622CCCF9297DA4FE229847C281D2A4B3737F7543::get_offset_of_colliders_11(),
	SpringBone_t622CCCF9297DA4FE229847C281D2A4B3737F7543::get_offset_of_debug_12(),
	SpringBone_t622CCCF9297DA4FE229847C281D2A4B3737F7543::get_offset_of_threshold_13(),
	SpringBone_t622CCCF9297DA4FE229847C281D2A4B3737F7543::get_offset_of_springLength_14(),
	SpringBone_t622CCCF9297DA4FE229847C281D2A4B3737F7543::get_offset_of_localRotation_15(),
	SpringBone_t622CCCF9297DA4FE229847C281D2A4B3737F7543::get_offset_of_trs_16(),
	SpringBone_t622CCCF9297DA4FE229847C281D2A4B3737F7543::get_offset_of_currTipPos_17(),
	SpringBone_t622CCCF9297DA4FE229847C281D2A4B3737F7543::get_offset_of_prevTipPos_18(),
	SpringBone_t622CCCF9297DA4FE229847C281D2A4B3737F7543::get_offset_of_org_19(),
	SpringBone_t622CCCF9297DA4FE229847C281D2A4B3737F7543::get_offset_of_managerRef_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2504 = { sizeof (SpringCollider_tDE193DFAA6219FEF2706546DF578770FB3744C2E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2504[1] = 
{
	SpringCollider_tDE193DFAA6219FEF2706546DF578770FB3744C2E::get_offset_of_radius_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2505 = { sizeof (SpringManager_t6A624FA45C3EA6326C3B0A4B34EF92F7C41D15DE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2505[6] = 
{
	SpringManager_t6A624FA45C3EA6326C3B0A4B34EF92F7C41D15DE::get_offset_of_dynamicRatio_4(),
	SpringManager_t6A624FA45C3EA6326C3B0A4B34EF92F7C41D15DE::get_offset_of_stiffnessForce_5(),
	SpringManager_t6A624FA45C3EA6326C3B0A4B34EF92F7C41D15DE::get_offset_of_stiffnessCurve_6(),
	SpringManager_t6A624FA45C3EA6326C3B0A4B34EF92F7C41D15DE::get_offset_of_dragForce_7(),
	SpringManager_t6A624FA45C3EA6326C3B0A4B34EF92F7C41D15DE::get_offset_of_dragCurve_8(),
	SpringManager_t6A624FA45C3EA6326C3B0A4B34EF92F7C41D15DE::get_offset_of_springBones_9(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
